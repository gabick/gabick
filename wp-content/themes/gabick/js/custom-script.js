var $j = jQuery.noConflict();
const URL = location.host;
const CATEGORY_SLUG = 'category'
$j(document).ready(function(){
	ContactFormSubmit();
	footerCopyright();

    function footerCopyright() {
        var $footer = $j("#footer-info")
        var $date = new Date().getFullYear();
        $footer.append('<span class="copyright">'+'\u00A9'+' '+$date+'</span>')
    }
	
	function ContactFormSubmit() {
		$j('#formulaire form').submit(function( event ) {
			dataLayer.push({'event': 'contact-form-home'});
		});
	}
	
})