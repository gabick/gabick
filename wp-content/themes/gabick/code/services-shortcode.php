<?php

function services_list_function() {
    global $post;

    $html = "";

    $my_query = new WP_Query( array(
        'post_type' => 'services',
        'posts_per_page' => 12
    ));

//	$html .= "<div class='flex-container'>";
//		if( $my_query->have_posts() ) : while( $my_query->have_posts() ) : $my_query->the_post();
//		$image = get_field('icone');
//		$fontAwesomeIcon = get_field('icone_font_awesome');
//		$fontAwesomeStackBoolean = get_field('icone_font_awesome_stack_selection');
//		$html .= "<div class='service-items'>";
//		if(!empty($image)) {
//			$html .= "<img class=".'service-icone'." src=".$image['url']." alt=".$image['alt']. "/>";
//		} else if ($fontAwesomeStackBoolean && !empty($fontAwesomeIcon)) {
//			$fontAwesomeStackSize = get_field('icone_font_awesome_stack_size');
//			$fontAwesomeStackIcon = get_field('icone_font_awesome_stack');
//			$html .="<div class='wrapper-font-awesome'>";
//			$html .="<span class='fa-stack fa-".$fontAwesomeStackSize."x'>";
//				$html .="$fontAwesomeIcon";
//				$html .="$fontAwesomeStackIcon";
//			$html .="</span>";
//			$html .="</div>";
//		} else if (!empty($fontAwesomeIcon)) {
//			$html .= "<i class=".$fontAwesomeIcon."></i>";
//		}
//			$html .= "<div class='service-text-container'>";
//				$html .= "<h3 class='service-title'>" . get_the_title() . " </h3>";
//				$html .= "<p class='service-text'>" . get_the_excerpt() . " </p>";
//			$html .= "</div>";
//		$html .= "</div>";
//		endwhile;
//	$html .= "</div>";


    $html .= "<div class='flex-container'>";
    if( $my_query->have_posts() ) : while( $my_query->have_posts() ) : $my_query->the_post();
        $image = get_field('icone');
        $fontAwesomeIcon = get_field('icone_font_awesome_select');
        $fontAwesomeStackBoolean = get_field('icone_font_awesome_stack_selection');
        $html .= "<div class='service-items'>";
        if(!empty($image)) {
            $html .= "<img class=".'service-icone'." src=".$image['url']." alt=".$image['alt']. "/>";
        }
//            else if (!empty($fontAwesomeIcon)) {
//                $fontAwesomeStackSize = get_field('icone_font_awesome_stack_size');
//                $fontAwesomeStackIcon = get_field('icone_font_awesome_stack');
//                $html .="<div class='wrapper-font-awesome'>";
//                $html .="<span class='fa-stack fa-".$fontAwesomeStackSize."x'>";
//                $html .="$fontAwesomeIcon";
//                $html .="$fontAwesomeStackIcon";
//                $html .="</span>";
//                $html .="</div>";
//            }
        else if (!empty($fontAwesomeIcon)) {
            $html .= "<i class=".$fontAwesomeIcon."></i>";
        }
        $html .= "<div class='service-text-container'>";
        $html .= "<h3 class='service-title'>" . get_the_title() . " </h3>";
        $html .= "<p class='service-text'>" . get_the_excerpt() . " </p>";
        $html .= "</div>";
        $html .= "</div>";
    endwhile;
        $html .= "</div>";
        wp_reset_postdata();
    endif;

    return $html;
}

function register_shortcodes(){
    add_shortcode('ServicesList', 'services_list_function');
}
add_action( 'init', 'register_shortcodes');
