<?php
/*
Plugin Name: Gabick Font Awesome
Plugin URI: https://gabick.com
description: A plugin to use font awesome into custom dropdown ACF and spread joy
Version: 1.0
Author: Gabick
Author URI: https://gabick.com
License: GPL2
*/

if ( ! defined( 'ABSPATH' ) ) {
    die( 'Invalid request.' );
}

if ( ! class_exists( 'Gabick_Font_Awesome' ) ) :
    class Gabick_Font_Awesome
    {
        private function __construct() {}

        public static function init_actions() {
            // acf/load_field/key={$field_key} - filter for a specific field based on it's key name , CHANGE THIS TO YOUR FIELDS KEY!
//            add_filter('acf/load_field/name=icone_font_awesome_select', array( __CLASS__,'acf_load_font_awesome_options'));
        }

        public static function acf_load_font_awesome_options( $field ) {
            //Change this to whatever data you are using.
            $data = Gabick_Font_Awesome::constructFontAwsomeArrayFromJson();

            $field['choices'] = array();

            //Loop through whatever data you are using, and assign a key/value
            foreach($data as $field_key => $field_value) {
                $code = '&#x'.$field_value['unicode'].';';
                $field['choices'][$field_value['class']] = $code.' '.$field_value['class'];
            }
            return $field;
        }

        public static function getdataFromJson () {
            $domain = Gabick_Font_Awesome::siteURL();
            $file = file_get_contents($domain.'/wp-content/plugins/gabick-font-awesome/json/font-awesome.json');
            $json = json_decode($file, true);
            return $json;
        }

        public static function constructFontAwsomeArrayFromJson () {
            $array = Gabick_Font_Awesome::getdataFromJson();
            $newArray = array();
            $type = '';
            foreach ($array as $key => $typeArray) {
                foreach ($typeArray as $newKey => $data){
                    if ($key == 'solid') {
                        $type = 'fas';
                    } elseif ($key == 'regular') {
                        $type = 'fab';
                    } elseif ($key == 'brand') {
                        $type = 'fao';
                    }
                    $info = explode(":", $data);
                    $concat = $type. ' fa-'.$info[0];
//                    array_push($newArray, $concat);
                    $newArray[$newKey]['class'] = $concat;
                    $newArray[$newKey]['unicode'] = $info[1];
                }
            }
            return $newArray;

        }

        public static function siteURL()
        {
            $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
            $domainName = $_SERVER['HTTP_HOST'].'/';
            return $protocol.$domainName;
        }
    }
    add_action( 'plugins_loaded', array( 'Gabick_Font_Awesome', 'init_actions' ) );
endif;
?>